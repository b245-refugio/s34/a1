const express = require("express");

const port = 4000;

const app = express();

app.use(express.json())

// mockdatabase
let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

app.get("/home", (req,res)=>{
    res.send("Welcome to homepage!")
})

app.get("/items", (req, res)=>{
    res.send(items)
})

app.delete("/delete-item", (req,res)=>{

    let deleteItem = items.pop()
    res.send(deleteItem);
})







app.listen(port, () => console.log(`Server is running at localhost ${port}`));